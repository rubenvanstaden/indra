# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.5

# Default target executed when no arguments are given to make.
default_target: all

.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/thedon/volundr/indra

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/thedon/volundr/indra

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "No interactive CMake dialog available..."
	/usr/bin/cmake -E echo No\ interactive\ CMake\ dialog\ available.
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache

.PHONY : edit_cache/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache

.PHONY : rebuild_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/thedon/volundr/indra/CMakeFiles /home/thedon/volundr/indra/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/thedon/volundr/indra/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean

.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named indra

# Build rule for target.
indra: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 indra
.PHONY : indra

# fast build rule for target.
indra/fast:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/build
.PHONY : indra/fast

src/bool.o: src/bool.cpp.o

.PHONY : src/bool.o

# target to build an object file
src/bool.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/bool.cpp.o
.PHONY : src/bool.cpp.o

src/bool.i: src/bool.cpp.i

.PHONY : src/bool.i

# target to preprocess a source file
src/bool.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/bool.cpp.i
.PHONY : src/bool.cpp.i

src/bool.s: src/bool.cpp.s

.PHONY : src/bool.s

# target to generate assembly for a file
src/bool.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/bool.cpp.s
.PHONY : src/bool.cpp.s

src/connect.o: src/connect.cpp.o

.PHONY : src/connect.o

# target to build an object file
src/connect.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/connect.cpp.o
.PHONY : src/connect.cpp.o

src/connect.i: src/connect.cpp.i

.PHONY : src/connect.i

# target to preprocess a source file
src/connect.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/connect.cpp.i
.PHONY : src/connect.cpp.i

src/connect.s: src/connect.cpp.s

.PHONY : src/connect.s

# target to generate assembly for a file
src/connect.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/connect.cpp.s
.PHONY : src/connect.cpp.s

src/geo.o: src/geo.cpp.o

.PHONY : src/geo.o

# target to build an object file
src/geo.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/geo.cpp.o
.PHONY : src/geo.cpp.o

src/geo.i: src/geo.cpp.i

.PHONY : src/geo.i

# target to preprocess a source file
src/geo.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/geo.cpp.i
.PHONY : src/geo.cpp.i

src/geo.s: src/geo.cpp.s

.PHONY : src/geo.s

# target to generate assembly for a file
src/geo.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/geo.cpp.s
.PHONY : src/geo.cpp.s

src/graph.o: src/graph.cpp.o

.PHONY : src/graph.o

# target to build an object file
src/graph.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/graph.cpp.o
.PHONY : src/graph.cpp.o

src/graph.i: src/graph.cpp.i

.PHONY : src/graph.i

# target to preprocess a source file
src/graph.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/graph.cpp.i
.PHONY : src/graph.cpp.i

src/graph.s: src/graph.cpp.s

.PHONY : src/graph.s

# target to generate assembly for a file
src/graph.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/graph.cpp.s
.PHONY : src/graph.cpp.s

src/induct.o: src/induct.cpp.o

.PHONY : src/induct.o

# target to build an object file
src/induct.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/induct.cpp.o
.PHONY : src/induct.cpp.o

src/induct.i: src/induct.cpp.i

.PHONY : src/induct.i

# target to preprocess a source file
src/induct.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/induct.cpp.i
.PHONY : src/induct.cpp.i

src/induct.s: src/induct.cpp.s

.PHONY : src/induct.s

# target to generate assembly for a file
src/induct.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/induct.cpp.s
.PHONY : src/induct.cpp.s

src/main.o: src/main.cpp.o

.PHONY : src/main.o

# target to build an object file
src/main.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/main.cpp.o
.PHONY : src/main.cpp.o

src/main.i: src/main.cpp.i

.PHONY : src/main.i

# target to preprocess a source file
src/main.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/main.cpp.i
.PHONY : src/main.cpp.i

src/main.s: src/main.cpp.s

.PHONY : src/main.s

# target to generate assembly for a file
src/main.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/main.cpp.s
.PHONY : src/main.cpp.s

src/mesh.o: src/mesh.cpp.o

.PHONY : src/mesh.o

# target to build an object file
src/mesh.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/mesh.cpp.o
.PHONY : src/mesh.cpp.o

src/mesh.i: src/mesh.cpp.i

.PHONY : src/mesh.i

# target to preprocess a source file
src/mesh.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/mesh.cpp.i
.PHONY : src/mesh.cpp.i

src/mesh.s: src/mesh.cpp.s

.PHONY : src/mesh.s

# target to generate assembly for a file
src/mesh.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/mesh.cpp.s
.PHONY : src/mesh.cpp.s

src/print.o: src/print.cpp.o

.PHONY : src/print.o

# target to build an object file
src/print.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/print.cpp.o
.PHONY : src/print.cpp.o

src/print.i: src/print.cpp.i

.PHONY : src/print.i

# target to preprocess a source file
src/print.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/print.cpp.i
.PHONY : src/print.cpp.i

src/print.s: src/print.cpp.s

.PHONY : src/print.s

# target to generate assembly for a file
src/print.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/print.cpp.s
.PHONY : src/print.cpp.s

src/read.o: src/read.cpp.o

.PHONY : src/read.o

# target to build an object file
src/read.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/read.cpp.o
.PHONY : src/read.cpp.o

src/read.i: src/read.cpp.i

.PHONY : src/read.i

# target to preprocess a source file
src/read.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/read.cpp.i
.PHONY : src/read.cpp.i

src/read.s: src/read.cpp.s

.PHONY : src/read.s

# target to generate assembly for a file
src/read.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/read.cpp.s
.PHONY : src/read.cpp.s

src/tools.o: src/tools.cpp.o

.PHONY : src/tools.o

# target to build an object file
src/tools.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/tools.cpp.o
.PHONY : src/tools.cpp.o

src/tools.i: src/tools.cpp.i

.PHONY : src/tools.i

# target to preprocess a source file
src/tools.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/tools.cpp.i
.PHONY : src/tools.cpp.i

src/tools.s: src/tools.cpp.s

.PHONY : src/tools.s

# target to generate assembly for a file
src/tools.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/tools.cpp.s
.PHONY : src/tools.cpp.s

src/vectors.o: src/vectors.cpp.o

.PHONY : src/vectors.o

# target to build an object file
src/vectors.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/vectors.cpp.o
.PHONY : src/vectors.cpp.o

src/vectors.i: src/vectors.cpp.i

.PHONY : src/vectors.i

# target to preprocess a source file
src/vectors.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/vectors.cpp.i
.PHONY : src/vectors.cpp.i

src/vectors.s: src/vectors.cpp.s

.PHONY : src/vectors.s

# target to generate assembly for a file
src/vectors.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/vectors.cpp.s
.PHONY : src/vectors.cpp.s

src/vtk_triangle.o: src/vtk_triangle.cpp.o

.PHONY : src/vtk_triangle.o

# target to build an object file
src/vtk_triangle.cpp.o:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/vtk_triangle.cpp.o
.PHONY : src/vtk_triangle.cpp.o

src/vtk_triangle.i: src/vtk_triangle.cpp.i

.PHONY : src/vtk_triangle.i

# target to preprocess a source file
src/vtk_triangle.cpp.i:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/vtk_triangle.cpp.i
.PHONY : src/vtk_triangle.cpp.i

src/vtk_triangle.s: src/vtk_triangle.cpp.s

.PHONY : src/vtk_triangle.s

# target to generate assembly for a file
src/vtk_triangle.cpp.s:
	$(MAKE) -f CMakeFiles/indra.dir/build.make CMakeFiles/indra.dir/src/vtk_triangle.cpp.s
.PHONY : src/vtk_triangle.cpp.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... edit_cache"
	@echo "... rebuild_cache"
	@echo "... indra"
	@echo "... src/bool.o"
	@echo "... src/bool.i"
	@echo "... src/bool.s"
	@echo "... src/connect.o"
	@echo "... src/connect.i"
	@echo "... src/connect.s"
	@echo "... src/geo.o"
	@echo "... src/geo.i"
	@echo "... src/geo.s"
	@echo "... src/graph.o"
	@echo "... src/graph.i"
	@echo "... src/graph.s"
	@echo "... src/induct.o"
	@echo "... src/induct.i"
	@echo "... src/induct.s"
	@echo "... src/main.o"
	@echo "... src/main.i"
	@echo "... src/main.s"
	@echo "... src/mesh.o"
	@echo "... src/mesh.i"
	@echo "... src/mesh.s"
	@echo "... src/print.o"
	@echo "... src/print.i"
	@echo "... src/print.s"
	@echo "... src/read.o"
	@echo "... src/read.i"
	@echo "... src/read.s"
	@echo "... src/tools.o"
	@echo "... src/tools.i"
	@echo "... src/tools.s"
	@echo "... src/vectors.o"
	@echo "... src/vectors.i"
	@echo "... src/vectors.s"
	@echo "... src/vtk_triangle.o"
	@echo "... src/vtk_triangle.i"
	@echo "... src/vtk_triangle.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

