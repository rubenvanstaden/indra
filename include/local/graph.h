#ifndef GRAPH_H
#define GRAPH_H

// Program to find Dijkstra's shortest path using
// priority_queue in STL
#include <bits/stdc++.h>
#include <stddef.h>
#include <armadillo>
#include "struct.h"

using namespace arma;
using namespace std;

# define INF 0x3f3f3f3f

// iPair ==>  Integer Pair
typedef pair<int, int> iPair;

// This class represents a directed graph using
// adjacency list representation
class Graph
{
    public:
        int V;

        // In a weighted graph, we need to store vertex
        // and weight pair for every edge
        list<iPair> *adj;

        int id;
        int type;

        Graph();
        ~Graph();

        void addEdge(int u, int v, int w);
        void shortest_path(System *sys, int s);
};

#endif
