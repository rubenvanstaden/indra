#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <vector>
#include <iostream>
#include <armadillo>
#include <stddef.h>
#include <iomanip>

#include "globals.h"
#include "args.h"
#include "logger.h"

using namespace std;
using namespace arma;

/**************************************************************
 * Hacker: 1h3d*n
 * For: Volundr
 * Docs: Main
 * Date: 19 April 2017
 *
 * This is the top header file in the herarchy. It includes the
 * main system struture of the program that holds all the main
 * info that will be used in the program.
 **************************************************************/

class Vertex;
class Triangle;
class Edge;

struct Node {
    int id;
    double x;
    double y;
    double z;
};

struct Terminal {
    int id;
    Node *node[2];
};

struct Params {
    bool debug;
};

struct System
{
    Args *par;

    int numVert;
    int numTri;
    int numEdge;
    int numTerm;

    double omega;

    sp_cx_mat L;
    sp_cx_mat R;
    sp_cx_mat Z;
    sp_cx_mat M;

    mat initMap;
    mat conMap;
    mat boundaryMap;
    mat edgeIdMap;

    vector<Vertex *> vecVert;
    vector<Triangle *> vecTri;
    vector<Edge *> vecEdge;
};

#endif
