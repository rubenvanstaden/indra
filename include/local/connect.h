#ifndef CONNECT_H
#define CONNECT_H

#include "bool.h"
#include "graph.h"
#include "print.h"

void connect_tri_to_nodes(System *sys);
void connect_tri_to_edges(System *sys, Logger *logger);
void connect_to_graph(System *sys, Graph *cGraph, Edge *edge, int n, int id);
void connect_edge_to_tri(System *sys, int index, int num, int id1, int id2);
void connect_center_to_tri_and_edge(Triangle *tri, int i, int j, CenterVector *cenVect);

#endif
