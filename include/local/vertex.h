#ifndef VERTEX_H
#define VERTEX_H

#include "struct.h"

class Vertex
{
    public:
        int id;
        int type;
        int divid_num;
        double curr_vector[2];
        Node *node;
        vector<Triangle *> tri;

        inline Vertex();
        inline ~Vertex();
};

inline Vertex::Vertex()
{
    node = new Node();
    divid_num = 0;
    curr_vector[0] = 0.0;
    curr_vector[1] = 0.0;
    type = TYPE_EMPTY;
}

inline Vertex::~Vertex() {}

#endif
