#ifndef EDGE_H
#define EDGE_H

#include <stddef.h>
#include "triangle.h"

class Edge
{
    public:
        int id;
        int type;
        bool graph_connect;
        Vertex *vertex[2];
        CenterVector *cenVect[2];
        Triangle *tri[2];

        inline Edge();
        inline ~Edge();

        inline void create_edge(int numEdge, int id1, int id2);
};

inline Edge::Edge()
{
    graph_connect = false;

    vertex[0] = new Vertex();
    vertex[1] = new Vertex();

    cenVect[0] = new CenterVector();
    cenVect[1] = new CenterVector();
    cenVect[0] = NULL;
    cenVect[1] = NULL;
}

inline Edge::~Edge() {}

inline void Edge::create_edge(int numEdge, int id1, int id2)
{
    id = numEdge + 1;
    type = TYPE_INTERNAL;
    vertex[0]->id = id1;
    vertex[1]->id = id2;
}



#endif
