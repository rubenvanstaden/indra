#ifndef VTK_WRITE_H
#define VTK_WRITE_H

#include <vtkPointData.h>
#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkTriangle.h>
#include <vtkPolyDataMapper.h>
#include <vtkFloatArray.h>

#include "vectors.h"
#include "print.h"

void vtk_triangle(System *sys, Logger *logger, cx_mat In);

#endif
