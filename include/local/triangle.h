#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <armadillo>
#include <vector>
#include <armadillo>

#include "cen_vect.h"

using namespace arma;
using namespace std;

// Save die corrspondng vertex se edge in the array index.
// Save verticex in rising order
class Triangle
{
    public:
        int id;
        double mArea;
        Node center;
        Vertex *vertex[3];
        CenterVector *cenVect[3];
        Edge *edge[3];

        inline Triangle();
        inline ~Triangle();

        void connect_edge_to_tri(int num, int v_tri_id_1, int v_tri_id_2);
};

inline Triangle::Triangle()
{
    cenVect[0] = new CenterVector();
    cenVect[1] = new CenterVector();
    cenVect[2] = new CenterVector();
}

inline Triangle::~Triangle() {}

#endif
