#ifndef PRINT_H
#define PRINT_H

#include "edge.h"
#include "logger.h"

void loadBar(int x, int n, int r, int w);
void progress_bar(int x, int n, int r, int w);
void print_terminal_edges(System *sys, Logger *logger);
void print_vertex(System *sys, Logger *logger);
void print_boundary_vertex(System *sys);
void print_edges(System *sys, Logger *logger);
void print_tri_edges(System *sys, Logger *logger);
void print_triangles(System *sys, Logger *logger);
void print_boundary_edges(System *sys);
void print_p_vector_edges(System *sys);
void print_edge_triangles(System *sys, Logger *logger);
void print_center_vectors(System *sys, Logger *logger);

#endif
