#ifndef INDUCTANCE_H
#define INDUCTANCE_H

#include "vectors.h"
#include "print.h"

void create_mzmt_matrix(System *sys, Logger *logger);
void impedance_matrix(System *sys, Logger *logger);
void create_resistance_matrix(System *sys, Logger *logger);
void create_inductance_matrix(System *sys, Logger *logger);
double calc_integral_m(Triangle *tri_m, Triangle *tri_n);
double calc_integral_n(Triangle *tri_n, Node rm);
Node calc_rm(Triangle *tri_m, int i);
Node calc_rn(Triangle *tri_n, int i);
double calc_f(Node rm, Node rn);

#endif
