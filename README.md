# tetrahenry

# Developement

- Header file herarchy:
    * Main -> Read.h
           -> Mesh.h
           -> Inductance.h
           -> Print.h

    * Edge.h -> Triangle.h -> CenterVector.h -> Vertex.h -> Structures.h
    * Connect.h -> Bool.h -> Edge.h
                -> Graph.h
    * Read.h -> Connect.h
    * Mesh.h -> Geometry.h -> Connect.h
                           -> Vector.h -> Edge.h
                           -> Print.h
    * Inductance.h -> Vector.h
    * Print.h -> Edge.h

<p align="center">
    <img alt="Alt Text" src="https://g.gravizo.com/source/custom_activity?https%3A%2F%2Fraw.githubusercontent.com%2FTLmaK0%2Fgravizo%2Fmaster%2FREADME.md" />
</p>
<!---
<details>
<summary></summary>
custom_activity
@startuml
start
:Hello world;
:This is on defined on
several **lines**;
stop
@enduml
custom_activity
</details>
-->
<!--![Alt text](https://g.gravizo.com/source/custom_activity?https%3A%2F%2Fraw.githubusercontent.com%2FTLmaK0%2Fgravizo%2Fmaster%2FREADME.md)-->
