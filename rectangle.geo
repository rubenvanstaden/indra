/*********************************************************************
 *
 *  Gmsh rectangle volume
 *
 *********************************************************************/

lc = 1.0e-6;

length = 1E-6;
width = 1E-6;
height = 0.5E-6;

Point(1) = {      0,     0, 0, lc};
Point(2) = {      0, width, 0, lc};
Point(3) = { length, width, 0, lc};
Point(4) = { length,     0, 0, lc};

Line(1) = {1,2};
Line(2) = {3,2};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(5) = {4,1,-2,3};

Plane Surface(6) = {5};

in = {1};
out = {3};

Physical Point(".f -s 1E-9 -e 1E9 -n 1") = {};
Physical Surface(".s -s 1E9 -l 0.0 -h 1E-9") = {6};

Physical Point(".e 0") = {};
Physical Line(".t -p 0") = in; // important: terminal's face must be connected through their edges
Physical Line(".t -n 0") = out;
