#include "vtk_write.h"

void vertex_currents(System *sys, Logger *logger, cx_mat In)
{
    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            int id = sys->vecTri[i]->edge[j]->id - 1;
            sys->vecTri[i]->vertex[j]->curr_vector[0] += real(In(id));
            sys->vecTri[i]->vertex[j]->curr_vector[1] += imag(In(id));
            sys->vecTri[i]->vertex[j]->divid_num++;
        }
    }

    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            if (sys->vecTri[i]->vertex[j]->divid_num > 0) {
                sys->vecTri[i]->vertex[j]->curr_vector[0] = sys->vecTri[i]->vertex[j]->curr_vector[0] / sys->vecTri[i]->vertex[j]->divid_num;
                sys->vecTri[i]->vertex[j]->curr_vector[1] = sys->vecTri[i]->vertex[j]->curr_vector[1] / sys->vecTri[i]->vertex[j]->divid_num;

                // Set back to zero, we have already found the
                // average current at this vertex.
                sys->vecTri[i]->vertex[j]->divid_num = 0;
            }
        }
    }
}

void vtk_triangle(System *sys, Logger *logger, cx_mat In)
{
    /**************************************************************
    * Unfortunately in this simple example the following lines are ambiguous.
    * The first 0 is the index of the triangle vertex which is ALWAYS 0-2.
    * The second 0 is the index into the point (geometry) array, so
    * this can range from 0-(NumPoints-1)
    * i.e. a more general statement is triangle->GetPointIds()->SetId(0, PointId);
    *
    * Notes
    * -----
    *    vertex_id <==> edge_id
    *
    *    Die vertex order in die triangle correspond met die
    *    edge order in die triangle, i.o.w. the edge[0] is the
    *    edge across vertex[0], and edge[1] is the edge across
    *    vertex[1], etc.
    **************************************************************/

    logger->printTitle("Writing VTK file");

    vertex_currents(sys, logger, In);

    vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
    vtkSmartPointer<vtkCellArray> triangles = vtkSmartPointer<vtkCellArray>::New();
    vtkSmartPointer<vtkTriangle> triangle = vtkSmartPointer<vtkTriangle>::New();
    vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
    vtkSmartPointer<vtkFloatArray> colors = vtkSmartPointer<vtkFloatArray>::New();
    vtkSmartPointer<vtkXMLPolyDataWriter> writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();

    colors->SetNumberOfComponents(3);
    colors->SetName("Colors");

    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            double x = sys->vecTri[i]->vertex[j]->node->x;
            double y = sys->vecTri[i]->vertex[j]->node->y;
            double z = sys->vecTri[i]->vertex[j]->node->z;

            double curr_x = sys->vecTri[i]->vertex[j]->curr_vector[0];
            double curr_y = sys->vecTri[i]->vertex[j]->curr_vector[1];

            float vertex[3] = {curr_x, curr_y, 0.0};

            points->InsertNextPoint(x,y,z);
            colors->InsertNextTupleValue(vertex);
        }

        cout << endl;
    }

    // Add the triangle to the list of triangles (in this case there is only 1)
    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++)
            triangle->GetPointIds()->SetId(j, 3*i + j);
        triangles->InsertNextCell(triangle);
    }

    // Add the geometry and topology to the polydata
    polydata->SetPoints(points);
    polydata->SetPolys(triangles);
    polydata->GetPointData()->SetVectors(colors);

    writer->SetFileName("current.vtp");
    writer->SetInputData(polydata);
    writer->Write();
}
