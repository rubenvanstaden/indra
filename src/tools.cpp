#include "tools.h"

void print_info(System *sys) 
{
    cout << endl;
    cout << endl;
    cout << "Running TriHenry:" << endl;
    cout << "   -- Version: " << "\033[1;32m" << VERSION << "\033[0m" << endl;
    cout << "   -- Date: " << DATE << endl;
    cout << "   -- Hacker: " << "\033[1;36m" << HACKER << "\033[0m" << endl;
    cout << "   -- Developer: " << DEVELOPERS << endl;
    cout << endl;
}

void set_options(System *sys, Logger *logger)
{
    int i;

    char cwd[1024];

#ifdef _WIN32
    if (_getcwd(cwd, sizeof(cwd)) != NULL)
        fprintf(stdout, "Current working dir: %s\n", cwd);
    else
        perror("getcwd() error");
#elif defined _WIN64
    if (_getcwd(cwd, sizeof(cwd)) != NULL)
        fprintf(stdout, "Current working dir: %s\n", cwd);
    else
        perror("getcwd() error");
#elif defined __unix__
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        logger->printTitle("Working directory");
        cout << "\033[1;31m" << cwd << "\033[0m" << endl;
    } else
        perror("getcwd() error");
#elif defined __APPLE__
    if (getcwd(cwd, sizeof(cwd)) != NULL)
        fprintf(stdout, "Current working dir: %s\n", cwd);
    else
        perror("getcwd() error");
#else
    printf("Unsupported operating system\n");
#endif
}

void write_time(int day)
{
    ofstream myfile;
    myfile.open ("day");
    myfile << day << endl;
    myfile.close();
}

void read_quote(int num)
{
    int count = 0;

    string line;
    string author = "";
    string quote = "";

    ifstream myfile ("quote");
    if (myfile.is_open()) {
        while(getline(myfile, line)) {
            if (line.empty())
                count++;
            else if (line[0] == '-') {
                if (count == num)
                    author = line;
            }
            else {
                if (count == num)
                    quote += line + "\n";
            }
        }
        myfile.close();
    }
    else cout << "Unable to open file";

    // Remove last new line char from quote.
    if (!quote.empty() && quote[quote.length()-1] == '\n') {
        quote.erase(quote.length()-1);
    }

    cout << '"' << "\033[1;33m" << quote << "\033[0m" << '"' << endl;
    cout << author << endl;
}

void create_quote()
{
    time_t t = time(NULL);
    tm* timePtr = localtime(&t);

    int saved_day = 0;

    srand((unsigned)time(0)); 
    int num_quote = rand() % 30;

    string line;
    ifstream myfile ("day");
    if (myfile.is_open()) {
        while ( getline (myfile,line) ) {
            string str(line);
            string buf;
            stringstream ss(str);

            vector<string> tokens;

            while (ss >> buf)
                tokens.push_back(buf);

            saved_day = atoi(tokens[0].c_str());
        }
        myfile.close();
    }
    else cout << "Unable to open file";

    if (saved_day == timePtr->tm_mday) {
        cout << endl;
        read_quote(num_quote);
        saved_day = timePtr->tm_mday;
        write_time(saved_day);
    }

    //cout << "seconds= " << timePtr->tm_sec << endl;
    //cout << "minutes = " << timePtr->tm_min << endl;
    //cout << "hours = " << timePtr->tm_hour << endl;
    //cout << "day of month = " << timePtr->tm_mday << endl;
    //cout << "month of year = " << timePtr->tm_mon << endl;
    //cout << "year = " << timePtr->tm_year << endl;
    //cout << "weekday = " << timePtr->tm_wday << endl;
    //cout << "day of year = " << timePtr->tm_yday << endl;
    //cout << "daylight savings = " << timePtr->tm_isdst << endl;
}

