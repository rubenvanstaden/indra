#include "graph.h"

    // Create a priority queue to store vertices that
    // are being preprocessed. This is weird syntax in C++.
    // Refer below link for details of this syntax
    // http://geeksquiz.com/implement-min-heap-using-stl/

    // Create a vector for distances and initialize all
    // distances as infinite (INF)

    // Insert source itself in priority queue and initialize
    // its distance as 0.

    /* Looping till priority queue becomes empty (or all
      distances are not finalized) */

    // The first vertex in pair is the minimum distance
    // vertex, extract it from priority queue.
    // vertex label is stored in second of pair (it
    // has to be done this way to keep the vertices
    // sorted distance (distance must be first item
    // in pair)

    // This is the first and last terminal
    // edge the loop is going thought.

Graph::Graph()
{
    adj = new list<iPair> [4];
}

Graph::~Graph() {}

void Graph::addEdge(int u, int v, int w)
{
    adj[u].push_back(make_pair(v, w));
    adj[v].push_back(make_pair(u, w));
}

void terminal_mesh(int parent[], int savedWeight[], int j, rowvec &vecTerm)
{
    if (savedWeight[j] == 0)
        return;

    terminal_mesh(parent, savedWeight, parent[j], vecTerm);
    vecTerm(j) = savedWeight[j];
}

void Graph::shortest_path(System *sys, int src)
{
    priority_queue< iPair, vector <iPair> , greater<iPair> > pq;

    int parent[V];
    int savedWeight[V];

    for (int i = 0; i < V; i++) {
        parent[0] = -1;
        savedWeight[0] = 0;
    }

    vector<int> dist(V, INF);

    pq.push(make_pair(0, src));
    dist[src] = 0;

    while (!pq.empty()) {
        int u = pq.top().second;
        pq.pop();

        list< pair<int, int> >::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i) {
            int v = (*i).first;
            int weight = (*i).second;

            if (dist[v] > dist[u]) {
                parent[v] = u;
                savedWeight[v] = weight;
                dist[v] = dist[u];
                pq.push(make_pair(dist[v], v));
            }
        }
    }

    rowvec vecTerm;
    vecTerm.zeros(sys->numEdge);

    vecTerm(2) = -1;
    vecTerm(7) = 1;

    terminal_mesh(parent, savedWeight, V-1, vecTerm);

    cout.precision(0);
    cout.setf(ios::fixed);
    cout << right << setw(3);

    vecTerm.raw_print(cout, "terminal graph:");

    for (int i = 0; i < sys->numEdge; i++)
        sys->M(0,i) = vecTerm(i);
}
