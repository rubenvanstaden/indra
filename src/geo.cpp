#include "geo.h"

void construct_geometry(System *sys, Logger *logger)
{
    create_triangle_area(sys);
    create_triangle_center_nodes(sys, logger);
    create_triangle_center_vectors(sys, logger);
    connect_tri_to_edges(sys, logger);
}

void create_triangle_area(System *sys)
{
    for (int i = 0; i < sys->numTri; i++) {
        Node vect_1 = vector_sub(sys->vecTri[i]->vertex[1]->node,
                                 sys->vecTri[i]->vertex[0]->node);
        Node vect_2 = vector_sub(sys->vecTri[i]->vertex[2]->node,
                                 sys->vecTri[i]->vertex[0]->node);
        Node vect_3 = vector_cross(&vect_1, &vect_2);
        sys->vecTri[i]->mArea = vector_norm(&vect_3) / 2;
    }
}

void create_triangle_center_nodes(System *sys, Logger *logger)
{
    if (sys->par->debug == 1) {
        logger->printTitle("Triangle center nodes");
        cout << fixed << setprecision(6);
    }

    detect_boundary_edges(sys);

    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            sys->vecTri[i]->center.x += sys->vecTri[i]->vertex[j]->node->x;
            sys->vecTri[i]->center.y += sys->vecTri[i]->vertex[j]->node->y;
            sys->vecTri[i]->center.z += sys->vecTri[i]->vertex[j]->node->z;
        }

        sys->vecTri[i]->center.x /= 3.0;
        sys->vecTri[i]->center.y /= 3.0;
        sys->vecTri[i]->center.z /= 3.0;

        if (sys->par->debug == 1) {
            int id = sys->vecTri[i]->id;
            double x = sys->vecTri[i]->center.x/DIM;
            double y = sys->vecTri[i]->center.y/DIM;

            cout << "  - Tri " << id << ":" << "\t";
            cout << left
                 << setw(4) << x << "\t"
                 << setw(4) << y << "\n";
        }
    }
}

void create_triangle_center_vectors(System *sys, Logger *logger)
{
    for (int i = 0; i < sys->numTri; i++) {
        CenterVector *cenVect[3];

        int direct = get_center_direction(sys, i);

        for (int j = 0; j < 3; j++) {
            cenVect[j] = new CenterVector();
            cenVect[j]->id = sys->vecTri[i]->edge[j]->id;

            if (sys->vecTri[i]->edge[j]->type != TYPE_BOUNDARY) {
                if (is_center_negative(direct)) {
                    cenVect[j]->vector = vector_sub(sys->vecTri[i]->vertex[j]->node,
                                                    &sys->vecTri[i]->center);
                    cenVect[j]->direct = DIRECT_NEG;
                } else {
                    cenVect[j]->vector = vector_sub(&sys->vecTri[i]->center,
                                                    sys->vecTri[i]->vertex[j]->node);
                    cenVect[j]->direct = DIRECT_POS;
                }
            } else {
                Node boundary;

                boundary.x = 0.0;
                boundary.y = 0.0;
                boundary.z = 0.0;

                cenVect[j]->vector = boundary;
                cenVect[j]->direct = DIRECT_POS;
            }

            cenVect[j]->tri = sys->vecTri[i];
            connect_center_to_tri_and_edge(sys->vecTri[i], i, j, cenVect[j]);
        }
    }

    if (sys->par->debug)
        print_center_vectors(sys, logger);
}

void detect_boundary_edges(System *sys)
{
    for (int i = 0; i < sys->numVert; i++) {
        for (int j = 0; j < sys->numVert; j++) {
            int bVal = sys->boundaryMap(i,j);

            if (bVal != 0)
                if (sys->vecEdge[bVal-1]->type != TYPE_TERMINAL)
                    sys->vecEdge[bVal-1]->type = TYPE_BOUNDARY;
        }
    }

    if (sys->par->debug == 1) {
        cout << "--> Boundary edges: ";
        for (int i = 0; i < sys->numEdge; i++)
            if (sys->vecEdge[i]->type == TYPE_BOUNDARY)
                cout << sys->vecEdge[i]->id << " ";
        cout << endl;
    }

    //if (sys->par->debug)
        //print_boundary_edges(sys);
}

void detect_boundary_vertex(System *sys)
{
    for (int i = 0; i < sys->numVert; i++) {
        int num = sys->vecVert[i]->tri.size();

        for (int j = 0; j < num; j++) {
            Triangle *tri = sys->vecVert[i]->tri[j];

            for (int n = 0; n < 3; n++) {
                Edge *edge = tri->edge[n];

                if (sys->vecVert[i]->id == edge->vertex[0]->id) {
                    if (edge->type == TYPE_BOUNDARY)
                        sys->vecVert[i]->type = TYPE_BOUNDARY;
                    if (edge->type == TYPE_TERMINAL)
                        sys->vecVert[i]->type = TYPE_TERMINAL;
                }
                if (sys->vecVert[i]->id == edge->vertex[1]->id) {
                    if (edge->type == TYPE_BOUNDARY)
                        sys->vecVert[i]->type = TYPE_BOUNDARY;
                    if (edge->type == TYPE_TERMINAL)
                        sys->vecVert[i]->type = TYPE_TERMINAL;
                }
            }
        }

        if (sys->vecVert[i]->type == TYPE_EMPTY)
            sys->vecVert[i]->type = TYPE_INTERNAL;
    }
}

int get_center_direction(System *sys, int i)
{
    int direct = 0;

    for (int j = 0; j < 3; j++) {
        if (sys->vecTri[i]->edge[j]->type != TYPE_BOUNDARY) {
            if (sys->vecTri[i]->edge[j]->cenVect[0])
                direct = sys->vecTri[i]->edge[j]->cenVect[0]->direct;
            else if (sys->vecTri[i]->edge[j]->cenVect[1])
                direct = sys->vecTri[i]->edge[j]->cenVect[1]->direct;
        }
    }

    if (direct == DIRECT_POS)
        direct = DIRECT_NEG;
    else if (direct == DIRECT_NEG)
        direct = DIRECT_POS;

    return direct;
}
