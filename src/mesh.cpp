#include "mesh.h"

/**************************************************************
 * Hacker: 1h3d*n
 * For: Volundr
 * Docs: Algorithm 5
 * Date: 19 April 2017
 *
 * Description: Calculating the internal mesh matrix, M.
 *
 * 1) A vector is created in the Class Mesh that contains
 *    all the internal nodes of the structure. These nodes
 *    form the vertices of the triangles that meshes the
 *    structure.
 *
 * 2) Loop throught each node that is not a boundary or
 *    terminal node and get the first triangle
 *    that shares a vertex with this node. That means the
 *    triangle is connected to the node. This is called
 *    the main triangle. And since it is the first traingle
 *    we will also save this as the orgin triangle, so we
 *    can know when we have moved 360 degrees around the node.
 *
 * 3) Loop throught the edges of this triangle and look at
 *    each edge's two connected triangles.
 *
 * 4) Move to the triangle of the edge that's ID is not
 *    equal to the main triangle's. Call this the edge triangle.
 *
 * 5) Make sure this edge triangle is also connected to the node.
 *
 * 6) The main triangle will now be the first vertex of
 *    the graph and the edge triangle will be the second.
 *
 * 7) The edge in the Class Graph that connects, main
 *    triangle and edge triangle, will be a 1/-1 depending
 *    on whether the main triangle is the edge's positive
 *    or negative triangle.
 *
 * 8) Once the two vertices and the edge is saved in the
 *    graph object, we can move the main triangle to be
 *    the edge's triangle. Then repeat this process to add
 *    to the graph object, untill we get the first triangle
 *    again.
 *
 * Creating terminal mesh row vectors:
 *
 * 1) Connect graph node of current triangle to other graph
 *    nodes connected to this triangle.
 *
 * 2) Look at the triangle that is not the current one we
 *    are looking at.
 *
 * 3) The first triangle in the edge is the positive one.
 *    If n==1 it means the main triangle is the positive
 *    one and that we are moving from the positive to the
 *    negative triangle connected to the edge.
 **************************************************************/

void create_internal_mesh(System *sys, Logger *logger);
void create_terminal_mesh(System *sys, Logger *logger);

Edge *get_new_edge_connected_to_vertex(Edge *edge, Triangle *tri);
Triangle *move_to_next_traingle(Triangle *tri);
void jump_over_edge_to_triangle(System *sys, vector<Triangle *> &tri_added, Triangle *tri, Edge *edge, Vertex *vertex, int cols, rowvec &row);

// This is the M matrix
void mesh_matrix(System *sys, Logger *logger)
{
    sys->M.resize(2,8);

    // geo.cpp
    detect_boundary_vertex(sys);

    create_internal_mesh(sys, logger);
    create_terminal_mesh(sys, logger);

    if (sys->par->debug) {
        cout << fixed << setprecision(0);
        cout << right << setw(3);

        cx_mat Mp(sys->M);
        Mp.raw_print(cout, "M:");
    }
}

void create_internal_mesh(System *sys, Logger *logger)
{
    logger->printTitle("Create internal mesh graph");

    for (int i = 0; i < sys->numVert; i++) {
        if (sys->vecVert[i]->type == TYPE_INTERNAL) {
            Vertex *vertex = sys->vecVert[i];
            Triangle *tri = sys->vecVert[i]->tri[0];

            vector<Triangle *> tri_added;

            Edge *edge = new Edge();
            edge = NULL;

            for (int n = 0; n < 3; n++) {
                edge = tri->edge[n];
                edge = get_new_edge_connected_to_vertex(tri->edge[n], tri);

                if (edge != NULL)
                    break;
            }

            int cols = 0;
            rowvec row;
            row.zeros(8);

            if (edge != NULL)
                jump_over_edge_to_triangle(sys, tri_added, tri, edge, vertex, cols, row);

            if (sys->par->debug == 1) {
                cout << "Tri id --> ";
                for (int i = 0; i < (int)tri_added.size(); i++)
                    cout << tri_added[i]->id << " ";
                cout << endl;
            }

            for (int j = 0; j < 8; j++) {
                sys->M(1,j) = row(j);
            }
        }
    }
}

void create_terminal_mesh(System *sys, Logger *logger)
{
    logger->printTitle("Create terminal mesh graph");

    Graph *cGraph = new Graph;
    cGraph->V = sys->numTri;

    for (int i = 0; i < sys->numTri; i++) {
        if (is_terminal_triangle(sys->vecTri[i]))
            cGraph->type = TYPE_TERMINAL;

        for (int j = 0; j < 3; j++){
            Edge *edge = sys->vecTri[i]->edge[j];

            if (edge->type == TYPE_INTERNAL) {
                for (int n = 0; n < 2; n++) {
                    if (edge->tri[n] != NULL) {
                        if (sys->vecTri[i]->id != edge->tri[n]->id) {
                            connect_to_graph(sys, cGraph, edge, n, sys->vecTri[i]->id);
                            edge->graph_connect = true;
                        }
                    }
                }
            }
        }
    }

    cGraph->shortest_path(sys, 0);
}

void jump_over_edge_to_triangle(System *sys, vector<Triangle *> &tri_added, Triangle *tri, Edge *edge, Vertex *vertex, int cols, rowvec &row)
{
    if (!is_triangle_reached(tri_added, tri)) {
        tri_added.push_back(tri);

        if (is_current_triangle_pos(edge, tri)) {
            row(edge->id-1) = 1;
            tri = move_to_next_traingle(edge->tri[1]);
            edge = get_new_edge_connected_to_vertex(edge, tri);
            jump_over_edge_to_triangle(sys, tri_added, tri, edge, vertex, cols, row);
        }
        if (is_current_triangle_neg(edge, tri)) {
            row(edge->id-1) = -1;
            tri = move_to_next_traingle(edge->tri[0]);
            edge = get_new_edge_connected_to_vertex(edge, tri);
            jump_over_edge_to_triangle(sys, tri_added, tri, edge, vertex, cols, row);
        }
    }
}

Triangle *move_to_next_traingle(Triangle *tri)
{
    Triangle *tri_new = new Triangle();
    tri_new = tri;
    return tri_new;
}

Edge *get_new_edge_connected_to_vertex(Edge *edge, Triangle *tri)
{
    Edge *edge_new = new Edge();
    edge_new = NULL;

    for (int i = 0; i < 3; i++) {
        if (tri->edge[i]->type == TYPE_INTERNAL) {
            if (is_new_edge_equal_to_old_edge(edge, tri->edge[i])) {
                edge_new = tri->edge[i];
                break;
            }
        }
    }

    return edge_new;
}
