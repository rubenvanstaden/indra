#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <time.h>

#include "tools.h"
#include "read.h"
#include "mesh.h"
#include "induct.h"
#include "print.h"
#include "logger.h"
#include "args.h"
#include "types.h"
#include "vtk_write.h"

/**************************************************************
 * Hacker: 1h3d*n
 * For: Volundr
 * Docs: Main
 * Date: 19 April 2017
 *
 * Installations
 * -------------
 *     VTK --> http://kazenotaiyo.blogspot.co.za/2010/06/installing-vtk-in-ubuntu-and-making.html
 *
 * Termnology
 * ----------
 *     --> Nodes are all the lose triangle vertices in the structure.
 *     --> Edge is the edge connecting two nodes.
 *     --> Trianlge is the polygon constructed by three edges.
 *     --> Vertex is the nodes connected to that triangle.
 *
 * Notes
 * -----
 *     1) The first center vector in the edge is the positive vector
 *        and the second is the negative.
 *     2) The first triangle connected to the edge is the edge's
 *        positive triangle.
 **************************************************************/

void start_program(System *sys, Logger *logger);
void end_program(System *sys, Logger *logger);

int main(int argc, char *argv[])
{
    long long int s = PAPI_get_real_cyc();

    System *sys = new System;
    Logger *logger = new Logger;
    sys->par = new Args(argc, argv);

    if (sys->par->verbose) {
        logger->verbose = true;
    }

    start_program(sys, logger);

    read_gmsh(sys, logger);
    construct_geometry(sys, logger);

    mesh_matrix(sys, logger);
    impedance_matrix(sys, logger);
    create_mzmt_matrix(sys, logger);

    end_program(sys, logger);

    long long int e = PAPI_get_real_cyc();
    printf("Wallclock cycles: %lld\eni\n\n", e-s);

    return 0;
}

void start_program(System *sys, Logger *logger)
{
    print_info(sys);
    create_quote();
    set_options(sys, logger);

    logger->printTitle("Parameters");
    sys->par->print(logger->stringLength, 0);
    logger->startTimer("Total FMM");
    logger->startPAPI();
}

void end_program(System *sys, Logger *logger)
{
    logger->printTitle("Total runtime");
    logger->stopPAPI();
    logger->stopTimer("Total FMM");
    logger->printPAPI();
}
