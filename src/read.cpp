#include "read.h"

void read_gmsh(System *sys, Logger *logger)
{
    logger->printTitle("Read nodes");

    string line;
    ifstream myfile ("rectangle.msh");

    int count = 0;
    sys->numVert = 0;
    sys->numTri = 0;
    sys->numEdge = 0;

    bool readVertex = false;
    bool readTriangle = false;
    bool readTerminals = false;

    vector<int> visited_nodes;
    vector<Node> terminal_nodes;

    if (myfile.is_open()) {
        while (getline(myfile, line)) {
            stringstream ss(line);
            string word;

            if (line.compare("$EndNodes") == 0)
                readVertex = false;
            if (line.compare("$EndElements") == 0) {
                readTriangle = false;
                readTerminals = false;
            }

            if (readVertex)
                split_line_vertex(sys, line);
            if (readTriangle)
                split_line_triangle(sys, line, visited_nodes);
            if (readTerminals)
                split_line_terminals(sys, line, terminal_nodes);

            for (int wordNum = 1; ss >> word; wordNum++) {
                if (word.compare("$Nodes") == 0) {
                    getline(myfile, line);
                    sys->numVert = atoi(line.c_str());
                    readVertex = true;
                }
                if (word.compare("$Elements") == 0) {
                    getline(myfile, line);
                    readTriangle = true;
                    readTerminals = true;
                }
            }
        }

        myfile.close();
    } else
        cout << "Unable to open file";

    read_edges(sys, logger, terminal_nodes);

    if (sys->par->debug) {
        print_vertex(sys, logger);
        print_triangles(sys, logger);
    }
}

// The -1 is to accoutn for the indexing in colMap/initmap
// change this asap mother fcukers.
void read_edges(System *sys, Logger *logger, vector<Node> terminal_nodes)
{
    //logger->printTitle("Read edges");

    for (int i = 0; i < sys->numTri; i++) {
        vector<int> vectVertID = sort_vertex_ids(sys, i);

        int id_1 = vectVertID[0];
        int id_2 = vectVertID[1];
        int id_3 = vectVertID[2];

        add_edge_to_mesh(sys, i, 0, id_2, id_3);
        add_edge_to_mesh(sys, i, 1, id_1, id_3);
        add_edge_to_mesh(sys, i, 2, id_1, id_2);

        connect_edge_to_tri(sys, i, 0, id_2, id_3);
        connect_edge_to_tri(sys, i, 1, id_1, id_3);
        connect_edge_to_tri(sys, i, 2, id_1, id_2);
    }

    create_map_matrix(sys);
    detect_terminal_edges(sys, logger, terminal_nodes);

    if (sys->par->debug)
        print_edges(sys, logger);
}

void split_line_vertex(System *sys, string line)
{
    string str(line);
    string buf;
    stringstream ss(str);

    vector<string> tokens;

    while (ss >> buf)
        tokens.push_back(buf);

    Vertex *vert = new Vertex();
    vert->id = atoi(tokens[0].c_str());
    vert->node->x = atof(tokens[1].c_str());
    vert->node->y = atof(tokens[2].c_str());
    vert->node->z = atof(tokens[3].c_str());

    sys->vecVert.push_back(vert);
}

void split_line_triangle(System *sys, string line, vector<int> &visited_nodes)
{
    string str(line);
    string buf;
    stringstream ss(str);

    vector<string> tokens;

    while (ss >> buf)
        tokens.push_back(buf);

    if (atoi(tokens[1].c_str()) == 2) {
        Triangle *tri = new Triangle();
        tri->id = sys->numTri + 1;

        int vertex_id_1 = atoi(tokens[5].c_str()) - 1;
        int vertex_id_2 = atoi(tokens[6].c_str()) - 1;
        int vertex_id_3 = atoi(tokens[7].c_str()) - 1;

        tri->vertex[0] = sys->vecVert[vertex_id_1];
        tri->vertex[1] = sys->vecVert[vertex_id_2];
        tri->vertex[2] = sys->vecVert[vertex_id_3];

        sys->vecTri.push_back(tri);

        // Connect triangles to vertex objects
        if (!has_node_been_visited(visited_nodes, vertex_id_1)) {
            sys->vecVert[vertex_id_1]->tri.push_back(sys->vecTri[sys->numTri]);
            visited_nodes.push_back(vertex_id_1);
        }
        if (!has_node_been_visited(visited_nodes, vertex_id_2)) {
            sys->vecVert[vertex_id_2]->tri.push_back(sys->vecTri[sys->numTri]);
            visited_nodes.push_back(vertex_id_2);
        }
        if (!has_node_been_visited(visited_nodes, vertex_id_3)) {
            sys->vecVert[vertex_id_3]->tri.push_back(sys->vecTri[sys->numTri]);
            visited_nodes.push_back(vertex_id_3);
        }

        sys->numTri++;
    }
}

void split_line_terminals(System *sys, string line, vector<Node> &terminal_nodes)
{
    string str(line);
    string buf;
    stringstream ss(str);

    vector<string> tokens;

    while (ss >> buf)
        tokens.push_back(buf);

    // Read the nodes that are connected
    // to the different terminals.
    // Save the two edge vertices of
    // the terminal in the x and y
    // members of a node object.
    if (atoi(tokens[1].c_str()) == 1) {
        Node node;
        node.id = atoi(tokens[0].c_str());
        node.x = atof(tokens[5].c_str());
        node.y = atof(tokens[6].c_str());

        terminal_nodes.push_back(node);
    }
}

void detect_terminal_edges(System *sys, Logger *logger, vector<Node> terminal_nodes)
{
    for (int i = 0; i < sys->numEdge; i++) {
        int edge_node_1 = sys->vecEdge[i]->vertex[0]->id;
        int edge_node_2 = sys->vecEdge[i]->vertex[1]->id;

        for (int j = 0; j < terminal_nodes.size(); j++)
            if ((terminal_nodes[j].x == edge_node_1) || (terminal_nodes[j].x == edge_node_2))
                if ((terminal_nodes[j].y == edge_node_1) || (terminal_nodes[j].y == edge_node_2))
                    sys->vecEdge[i]->type = TYPE_TERMINAL;
    }

    if (sys->par->debug)
        print_terminal_edges(sys, logger);
}

void add_edge_to_mesh(System *sys, int index, int num, int id1, int id2)
{
    Edge *cEdge = new Edge();

    bool edge_list_zero = is_edge_list_zero(sys->numEdge);
    bool is_edge_in_list = does_edge_exist(sys->vecEdge, sys->numEdge, id1, id2);

    if (!is_edge_in_list || edge_list_zero) {
        cEdge->create_edge(sys->numEdge, id1, id2);
        sys->vecEdge.push_back(cEdge);
        sys->numEdge++;
    }
}

void create_map_matrix(System *sys)
{
    sys->initMap.zeros(sys->numVert, sys->numVert);
    sys->conMap.zeros(sys->numVert, sys->numVert);
    sys->boundaryMap.zeros(sys->numVert, sys->numVert);
    sys->edgeIdMap.zeros(sys->numVert, sys->numVert);

    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            int v1 = sys->vecTri[i]->edge[j]->vertex[0]->id - 1;
            int v2 = sys->vecTri[i]->edge[j]->vertex[1]->id - 1;

            if (sys->initMap(v1, v2) == 0)
                sys->initMap(v1, v2) = sys->vecTri[i]->id;
            else
                sys->conMap(v1, v2) = sys->vecTri[i]->id;

            if (sys->initMap(v2, v1) == 0)
                sys->initMap(v2, v1) = sys->vecTri[i]->id;
            else
                sys->conMap(v2, v1) = sys->vecTri[i]->id;
        }
    }

    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            int v1 = sys->vecTri[i]->edge[j]->vertex[0]->id - 1;
            int v2 = sys->vecTri[i]->edge[j]->vertex[1]->id - 1;

            if ((sys->initMap(v1,v2) != 0) && (sys->conMap(v1,v2) == 0))
                sys->boundaryMap(v1,v2) = sys->vecTri[i]->edge[j]->id;
            if ((sys->initMap(v2,v1) != 0) && (sys->conMap(v2,v1) == 0))
                sys->boundaryMap(v2,v1) = sys->vecTri[i]->edge[j]->id;
            if ((sys->initMap(v1,v2) != 0) && (sys->conMap(v1,v2) != 0))
                sys->edgeIdMap(v1,v2) = sys->vecTri[i]->edge[j]->id;
            if ((sys->initMap(v2,v1) != 0) && (sys->conMap(v2,v1) != 0))
                sys->edgeIdMap(v2,v1) = sys->vecTri[i]->edge[j]->id;
        }
    }

    if (sys->par->debug) {
        cout.precision(0);
        cout.setf(ios::fixed);
        cout << right << setw(3);

        sys->initMap.raw_print(cout, "ini_map:");
        cout << endl;
        sys->conMap.raw_print(cout, "con_map:");
        cout << endl;
        sys->boundaryMap.raw_print(cout, "boundary_map:");
        cout << endl;
        sys->edgeIdMap.raw_print(cout, "edge_id_map:");
    }
}

// Rearange the triangle vertices in rising order
vector<int> sort_vertex_ids(System *sys, int i)
{
    vector<int> vectVertID(3);

    vectVertID[0] = sys->vecTri[i]->vertex[0]->id;
    vectVertID[1] = sys->vecTri[i]->vertex[1]->id;
    vectVertID[2] = sys->vecTri[i]->vertex[2]->id;

    Vertex *temp[3];

    sort(vectVertID.begin(), vectVertID.end());

    for (int j = 0; j < 3; j++) {
        if (sys->vecTri[i]->vertex[j]->id == vectVertID[0])
            temp[0] = sys->vecTri[i]->vertex[j];
        if (sys->vecTri[i]->vertex[j]->id == vectVertID[1])
            temp[1] = sys->vecTri[i]->vertex[j];
        if (sys->vecTri[i]->vertex[j]->id == vectVertID[2])
            temp[2] = sys->vecTri[i]->vertex[j];
    }

    sys->vecTri[i]->vertex[0] = temp[0];
    sys->vecTri[i]->vertex[1] = temp[1];
    sys->vecTri[i]->vertex[2] = temp[2];

    vectVertID[0] = sys->vecTri[i]->vertex[0]->id;
    vectVertID[1] = sys->vecTri[i]->vertex[1]->id;
    vectVertID[2] = sys->vecTri[i]->vertex[2]->id;

    return vectVertID;
}
