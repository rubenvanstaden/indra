#include "connect.h"

void connect_to_graph(System *sys, Graph *cGraph, Edge *edge, int n, int id)
{
    for (int i = 0; i < sys->numTri; i++) {
        if (sys->vecTri[i]->id == edge->tri[n]->id) {
            if (!edge->graph_connect) {
                if (n == 0)
                    cGraph->addEdge(id-1, edge->tri[n]->id-1, -1);
                else
                    cGraph->addEdge(id-1, edge->tri[n]->id-1, 1);
            }
        }
    }
}

void connect_tri_to_edges(System *sys, Logger *logger)
{
    for (int i = 0; i < sys->numTri; i++) {
        for (int j = 0; j < 3; j++) {
            if (sys->vecTri[i]->cenVect[j]->direct == DIRECT_POS)
                sys->vecTri[i]->edge[j]->tri[0] = sys->vecTri[i];
            if (sys->vecTri[i]->cenVect[j]->direct == DIRECT_NEG)
                sys->vecTri[i]->edge[j]->tri[1] = sys->vecTri[i];
        }
    }

    if (sys->par->debug) {
        print_tri_edges(sys, logger);
        print_edge_triangles(sys, logger);
    }
}

void connect_edge_to_tri(System *sys, int index, int num, int id1, int id2)
{
    for (int i = 0; i < sys->numEdge; i++) {
        int edge_id_1 = sys->vecEdge[i]->vertex[0]->id;
        int edge_id_2 = sys->vecEdge[i]->vertex[1]->id;

        if ((edge_id_1 == id1) || (edge_id_1 == id2)) {
            if ((edge_id_2 == id2) || (edge_id_2 == id1))  {
                sys->vecTri[index]->edge[num] = sys->vecEdge[i];
                // cout << num << " : " << index << " : " << sys->vecEdge[i]->id << endl;
            }
        }
    }
}

void connect_center_to_tri_and_edge(Triangle *tri, int i, int j, CenterVector *cenVect)
{
    tri->cenVect[j] = cenVect;

    if (cenVect->direct != DIRECT_EMPTY) {
        if (is_center_negative(cenVect->direct))
            tri->edge[j]->cenVect[1] = cenVect;
        else
            tri->edge[j]->cenVect[0] = cenVect;
    } else {
        cout << "'ERROR: Why is center vector zero?" << endl;
    }
}
