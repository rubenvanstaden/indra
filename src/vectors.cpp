#include "vectors.h"

Node vector_mul_add(Node r, double Lx, Node *rx)
{
    r.x = r.x + Lx*rx->x;
    r.y = r.y + Lx*rx->y;
    r.z = r.z + Lx*rx->z;

    return r;
}

Node vector_cross(Node *vec1, Node *vec2)
{
    Node vec;

    vec.x = (vec1->y * vec2->z) - (vec1->z * vec2->y);
    vec.y = (vec1->z * vec2->x) - (vec1->x * vec2->z);
    vec.z = (vec1->x * vec2->y) - (vec1->y * vec2->x);

    return vec;
}

double vector_dot(Node pm, Node pn)
{
    double sum = 0;
    sum = pm.x*pn.x + pm.y*pn.y + pm.z*pn.z;
    return sum;
}

Node vector_sub(Node *vec1, Node *vec2)
{
    Node vec;

    vec.x = vec1->x - vec2->x;
    vec.y = vec1->y - vec2->y;
    vec.z = vec1->z - vec2->z;

    return vec;
}

double vector_inner_sum(vec V)
{
    double sum = V(0) + V(1) + V(2) + V(3);
    return sum;
}
double vector_norm(Node *vect)
{
    return sqrt(vect->x*vect->x + vect->y*vect->y + vect->z*vect->z);
}
